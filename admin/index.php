<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "config.php";
  echo
  '
  <form method="POST">
    <button class="submit" name="remoduls"> Restart moduls </button>
  </form>
  ';

  if(!empty($_POST["remoduls"])){
    unset($_SESSION["moduls_array"]);
    echo "DONE";
  }

/*  //PHP Mailer
  require_once '../resources/mailer/src/Exception.php';
  require_once '../resources/mailer/src/PHPMailer.php';

  $mail = new PHPMailer\PHPMailer\PHPMailer();
//  $mail = new PHPMailer(true);

//  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //$mail->isSendmail();
        //Recipients
        $mail->setFrom('h.kocvik@seznam.cz');
        $mail->addAddress('kimichisxd285@gmail.com');     // Add a recipient
        $mail->addReplyTo('kimichisxd285@gmail.com');

        //Attachments
        $mail->addAttachment('images/default_user.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      if(!$mail->send()){
        echo "Mailer Error: ". $mail->ErrorInfo;
      }else{
        echo 'Message has been sent';
      }
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
*/
  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);

  //var_dump($user);
?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../moduls/restaurace/css/recipe.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Administrace | M_CMS </title>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.sidenav').sidenav();
        $('.modal').modal();
      });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "resources/maintance/components/alerts.php";        require_once "resources/maintance/components/sidenav.php";
       ?>
       <div class="row">
         <div class="col s5 offset-s1">
           <h4> Vaše aktuality </h4>
           <?php
          /*  $myArts = $user->getMyLiveArticleFeed();
            foreach($myArts as $art){
              echo
              '
                <div class="section">
                  <h5> '.$art["Name"].' </h5>
                  <p> '.$art["Created"].' </h5>
                  <p> '.$art["Content"].'</h5>
                </div>
              ';
            }*/
            $ma = $user->getMyLiveArticleFeed();
            foreach($ma as $art){
              echo
              '
              <div class="row">
               <div class="col s12 m6">
                 <div class="card blue-grey darken-1" style="width: 500px !important;">
                   <div class="card-content white-text">
                     <span class="card-title">'.$art["Name"].'</span>
                     <span> '.$art["Created"].' </span>
                     <p>'.$art["Content"].'</p>
                   </div>
                   <div class="card-action">
                     <a href="#"> Pokračovat v článku </a>
                   </div>
                 </div>
               </div>
             </div>
             ';
            }

            ?>
         </div>
         <div class="col s4 offset-s1">
           <h4> Systémové zprávy </h4>
           <?php
          //  $sysArts = $user->getSystemArticleFeed();
          $myArts = $user->getMyLiveArticleFeed();
          foreach($myArts as $art){
            echo
            '
              <div class="section">
                <h5> '.$art["Name"].' </h5>
                <p> '.$art["Created"].' </h5>
                <p> '.$art["Content"].'</h5>
              </div>
            ';
          }
            ?>
         </div>
       </div>
    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
