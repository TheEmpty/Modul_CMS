<?php
  require_once "objects/app.php";
  require_once "objects/user.php";
  require_once "objects/modul.php";

  //Konfigurace aplikace, obsahuje kritické proměnné pro běh aplikace!
  define("URLINDEX", $_SERVER['HTTP_HOST']."/modul_cms"); //Lokace kořenové složky na localhostu
  define("NO_LOGIN_URL", $_SERVER['HTTP_HOST']."/modul_cms/index.php");
  define("URL_MODULS", $_SERVER["DOCUMENT_ROOT"].'/modul_cms'); //Moduly
  define("MODUL_PATH_FOR_EXCLUDE", "\modul_cms");

  /* **************** Další dodatečná konfigurace ****************** */
  ini_set("display_errors", TRUE);
  date_default_timezone_set( "Europe/Prague" );
  define("Actual_Date", date("Y-m-d"));

  /* ******************** Vlastní Exception Handler ************************ */
  function exceptions_handler($e){
    echo
    '
    <!DOCTYPE HTML>
      <html>
        <head>
           <title> Modul_cms | Kritická chyba aplikace </title>
           <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
           <!--Import materialize.css-->
           <link type="text/css" rel="stylesheet" href="resources/materialize/css/materialize.min.css"  media="screen,projection"/>
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
           <!--Let browser know website is optimized for mobile-->
           <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
           <meta charset="utf-8">
        </head>
        <body>
          <div class="row center">
            <div class="col s8 offset-s2">
              <h1 style="margin-top: 10%; text-decoration: underline"> Modul_CMS </h1>
              <i class="material-icons large"> error cloud_off flash_off highlight_off not_interested error_outline </i>
              <h4>
                Omlouváme se, ale byla zjištěna kritická chyba! Prosíme, počkejte do restartu služby, nebo kontaktujte administrátora.
              </h4>
              <div class="row red center-align" style="margin-top: 10%">
                <p>'. $e->getMessage() .'</p>
              </div>
            </div>
          </div>
        </body>
      </html>
    ';
    error_log($e->getMessage());
  }
  //Definice vlastního exception handleru
  set_exception_handler( "exceptions_handler" );

  /* **************** PDO Script ****************** */
  define("dbserver", "127.0.0.1"); //Adresa k databázovém serveru
  define("dbuser", "root"); //Uživatelské jméno
  define("dbpass", "mysql"); //Heslo uživatele
  define("dbname", "modul_cms"); //Název databáze

  $db = new PDO(
  "mysql:host=" .dbserver. ";dbname=" .dbname,dbuser,dbpass,
    array(
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8"
      )
  );
  /* ****************** Aktivace kritických funkcí ******************************** */
  if(empty($_SESSION["moduls_array"])){
    $_SESSION["moduls_array"] = initiateModuls($db); //Inicializace modulů
  }
  /* **************** Funkce lokalizující jednotlivé moduly **************************** */
  function initiateModuls($db){
    require_once "objects/modul.php";
    $modul_config = glob(URL_MODULS.'/moduls/*/modul_config.php');
    var_dump($modul_config);
    $moduls_array = array();
    foreach($modul_config as $cnf){
      require_once $cnf;
      $modul = new MODUL($modul_cnf, $db);
      $moduls_array[] = $modul->addInMenu();
    }
    return $moduls_array;
  }


?>
