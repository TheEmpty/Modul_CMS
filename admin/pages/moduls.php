<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../config.php";

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);

  if($user->show("level") == 10){ //user level
    require_once "../resources/maintance/pages/user/user_moduls.php";
  }elseif($user->show("level") == 1){ //admin level
    require_once "../resources/maintance/pages/admin/admin_moduls.php";
  }

  echo '<script type="text/javascript" src="../resources/materialize/js/materialize.min.js"></script>';
 ?>
