<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../config.php";

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);

  /*
  user = 1
  user-employee = 2
  user-admin = 3
  admin = 10

  */

  if($user->show("level") == 10){
    //require_once "../resources/maintance/pages/user/user_users.php";
  }elseif($user->show("level") == 1){
    require_once "../resources/maintance/pages/admin/admin_users.php";
  }

  echo '<script type="text/javascript" src="../resources/materialize/js/materialize.min.js"></script>';
?>
