<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../config.php";
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);
 ?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../resources/styles/profile.css"/>
    <link rel="stylesheet" type="text/css" href="../resources/styles/datepicker_modal.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Administrace | M_CMS </title>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.sidenav').sidenav();
          $('select').formSelect();
        });

        function init() {
          const datePickOpts = {
            autoClose: true,
            showDaysInNextAndPreviousMonths: true
          };

            M.Modal.init(document.querySelectorAll(".modal"));
            M.Datepicker.init(document.querySelectorAll(".datepicker"), datePickOpts);
          }

          document.addEventListener("DOMContentLoaded", init);

    </script>
  </head>
  <body>
    <div class="page row">
      <div class="col s12">
        <?php
          require_once "../resources/maintance/components/sidenav.php";
          require_once "../resources/maintance/components/alerts.php";
        ?>
        <div class="profile_container">
          <div class="col s4">
  			 	   <img id="userPic" class="right" src="../<?php echo $user->show('image')?>" />
  			 	</div>
          <div class="col s6">
          <?php
            echo '
              <h2> '. $user->show("fname") .' '. $user->show("sname") .' </h2>
              <hr>
              <table class="striped responsive_table">
              <!--  <tr>
                  <td> Křestní jméno: </td>
                  <td> '. $user->show("fname") .' </td>
                </tr>
                <tr>
                  <td> Příjmení: </td>
                  <td> '. $user->show("sname") .' </td>
                </tr> -->
                <tr>
                  <td> Email: </td>
                  <td> '. $user->show("email") .' </td>
                </tr>
                <tr>
                  <td> Pohlaví: </td>
                  <td> '. $user->show("gender") .' </td>
                </tr>
                <tr>
                  <td> Datum narození: </td>
                  <td> '. $user->show("birth") .' </td>
                </tr>
                <tr>
                  <td> IČO: </td>
                  <td> '. $user->show("ico") .'</td>
                </tr>
                <tr>
                  <td> DIČ: </td>
                  <td> '. $user->show("dic") .'</td>
                </tr>
                <tr>
                  <td> Společnost: </td>
                  <td> '. $user->show("company").'</td>
                </tr>
                <tr>
                  <td> Oprávnění: </td>
                  <td> '. $user->show("level") .'</td>
                </tr>
              </table>
            ';

            echo'
            <div id="userEditModal" class="modal modal-fixed-footer">
              <div class="modal-content">
                <h2> Upravit profil </h2>
                <div class="row">
                  <form class="col s8" action="../core.php" method="POST">
                    <div class="row">
                      <div class="input-field col s12">
                          <input type="email" name="email" id="email" value="'.$user->show("email").'"/>
                          <label for="email"> Email: </label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <select name="gender">
                          <option value="" disabled selected> Vyberte své pohlaví </option>
                          <option value="Muž"> Muž </option>
                          <option value="Žena"> Žena </option>
                          <option value="Jiné"> Jiné </option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <input type="text" id="birth" name="birth" class="datepicker" placeholder="Datum narození: "/>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s6">
                        <input type="number" name="ico" id="ico" value="'.$user->show("ico").'"/>
                        <label for="ico"> IČO: </label>
                      </div>
                      <div class="input-field col s6">
                        <input type="number" name="dic" id="dic" value="'.$user->show("dic").'"/>
                        <label for="dic"> DIČ: </label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input type="text" name="company" id="company" value="'.$user->show("company").'"/>
                        <label for="company"> Společnost: </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button class="btn waves-effect waves-light right" type="submit" name="action" value="editProfileUser"> Upravit profil</button>
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Zrušit</a>
                </div>
              </form>
            </div>
						';
           ?>
          </div>
          <div class="row">
	 					<div class="col s10">
	 						<a id="editFormButton" href="#userEditModal" class="waves-effect waves-light btn right modal-trigger profile-button orange darken-2"> Upravit profil </a>
	 					</div>
	 				</div>
        </div>
      </div>
    </div>
    <?php
      require_once "../resources/maintance/components/alerts.php";
     ?>
    <script type="text/javascript" src="../resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
