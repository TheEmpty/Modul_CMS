<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../config.php";

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);

 ?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Dashboard | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          M.AutoInit();
          $('textarea#content').characterCounter();
        });
    </script>
  </head>
  <body>
    <div class="col s12">
      <?php
        require_once "../resources/maintance/components/sidenav.php";
        require_once "../resources/maintance/components/alerts.php";
      ?>
      <div class="row">
        <form class="col s10 offset-s1" action="../core.php" method="POST">
          <div class="row">
            <div class="input-field col s6 offset-s1">
              <input type="text" id="name" name="name" class="validate" minlength="3"/>
              <label for="name" data-error="Pole musí obsahovat alespoň 3 symboly."> Název nového příspěvku </label>
            </div>
            <div class="row">
              <div class="input-field col s9 offset-s1">
                <textarea id="content" name="content" class="materialize-textarea" data-length="65000"></textarea>
                <label for="content"> Obsah příspěvku </label>
              </div>
            </div>
            <input type="hidden" name="uid" value="<?php echo $user->show('id'); ?>"/>
            <div class="row">
              <div class="input-field col s5">
                <?php
                if($user->show("level") == 1){ //admin level
                  echo
                  '
                    <select name="target" class="validate">
                      <option value="" disable selected> Cílová skupina </option>
                      <option value=""> Všichni </option>
                      <option value="Administrátoři"> Pouze administrátoři </option>
                    </select>
                  ';
                }elseif($user->show("level") == 2){ //user-admin level
                 echo
                 '
                  <select name="target" class="validate">
                    <option value="" disabled selected> Cílová skupina </option>
                    <option value=""> Všichni </option>
                    <option value="'.$user->show("company").'"> Pouze moje společnost </option>
                  </select>
                  ';
                }
                ?>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s5">
                <select name="status" class="validate">
                  <option value="" disable selected> Stav příspěvku </option>
                  <option value="Publikováno"> Publikovat </option>
                  <option value="Koncept"> Koncept </option>
                </select>
              </div>
            </div>
            <div class="row">
              <button class="btn right" type="submit" name="action" value="newArticle"> Vytvořit nový příspěvek </button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="../resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
