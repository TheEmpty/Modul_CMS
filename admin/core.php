<?php
  //Soubor s jádrem aplikace, obsahuje kritické funkce pro funkcionalitu a běh aplikace!!
  session_start();

  /* *****************Nutné komponenty jádra******************* */
  require_once "config.php";
  require_once "objects/user.php";
  require_once "objects/app.php";

  /* ****************Globální proměnné pro tento soubor pouze************* */
  $GLOBALS["app"] = new APP($db);

  /* ************************Získání dat z příchozích POST žádostí************** */
  $post_action = isset( $_POST['action'] ) ? $_POST['action'] : "";
  $post_email = isset( $_POST['email'] ) ? $_POST['email'] : "";
  $post_pswd = isset( $_POST['pswd'] ) ? $_POST['pswd'] : "";
  $post_firstname = isset( $_POST['firstname'] ) ? $_POST['firstname'] : "";
  $post_surname = isset( $_POST['surname'] ) ? $_POST['surname'] : "";
  $post_lvl = isset( $_POST['level'] ) ? $_POST['level'] : "";
  $post_dic = isset( $_POST['dic'] ) ? $_POST['dic'] : "";
  $post_ico = isset( $_POST['ico'] ) ? $_POST['ico'] : "";
  $post_date = isset( $_POST['date'] ) ? $_POST['date'] : "";
  $post_company = isset( $_POST['company'] ) ? $_POST['company'] : "";
  $post_gender = isset( $_POST['gender'] ) ? $_POST['gender'] : "";
  $post_uid = isset( $_POST['uid'] ) ? $_POST['uid'] : "";
  $post_name = isset( $_POST['name'] ) ? $_POST['name'] : "";
  $post_content = isset( $_POST['content'] ) ? $_POST['content'] : "";
  $post_target = isset( $_POST['target'] ) ? $_POST['target'] : "";
  $post_status = isset( $_POST['status'] ) ? $_POST['status'] : "";

  /* ************************Získání dat z příchozích GET žádostí****************** */
  $get_action = isset( $_GET['action'] ) ? $_GET['action'] : "";
  $get_aid = isset( $_GET['aid'] ) ? $_GET['aid'] : "";
  $get_uid = isset( $_GET['uid'] ) ? $_GET['uid'] : "";

  /* ******************Provedení požadované POST akce**************************** */
if(!empty($post_action)){
  switch ($post_action) {
    case 'login':
      login($post_email, $post_pswd);
      break;

    case 'registrate':
      registrate($post_firstname, $post_surname, $post_email, $post_pswd, $post_lvl);
      break;

    case 'logout':
      unset($_session["Logged_User"]);
      break;

    case 'editProfileUser':
      editProfileUser($post_email, $post_pswd, $post_gender, $post_date, $post_ico, $post_dic, $post_company);
      break;

    case 'deleteUser':
      deleteUser($post_uid);
      break;

    case 'newArticle':
      newArticle($post_uid, $post_name, $post_content, $post_target, $post_status);
      break;

     default:
      not_existing_action($post_action);
      break;
  }
}else{
  /* *******************Provedení požadované GET akce**************************** */
    switch ($get_action){
      case 'logout':
        logout();
        break;

      case 'delArt':
        deleteArticle($get_uid, $get_aid);
        break;

      default:
        not_existing_action($get_action);
        break;
    }
}
  /* ************Jednotlivé POST funkce************* */

//Funkce pro přihlášení uživatele do systému
  function login($mail, $pswd){
    if(!empty($mail) AND !empty($pswd)){
      $var = $GLOBALS["app"]->login_user($mail, $pswd);
      if($var != "wrong"){
        header("Location: index.php");
      }else{
        unset($_SESSION["error"]);
        $_SESSION["error"] = "Neshoda emailu nebo hesla!";
        header("Location: ../index.php");
      }
    }else{
      unset($_SESSION["error"]);
      $_SESSION["error"] = "Nebyla vyplněna všechna pole!";
      header("Location: ../index.php");
      die();
    }
  }

//Funkce pro registrace nového uživatele do systému
  function registrate($firstname, $surname, $email, $pswd, $lvl){
    if(!empty($firstname) AND !empty($surname) AND !empty($email) AND !empty($pswd) AND !empty($lvl)){
      $var = $GLOBALS["app"]->registrate_user($firstname, $surname, $email, $pswd, $lvl);
      if($var == "done"){
        unset($_session["succ"]);
        $_session["succ"] = "Registrace nového účtu proběhla úspěšně, přihlašte se.";
        header("Location: ../index.php");
        die();
      }
    }else{
      error("Všechna pole musí být vyplněna!");
    }
  }

  //Funkce pro změnu údajů profilu
  function editProfileUser($email, $pswd, $gender, $date, $ico, $dic, $company){
    $user = unserialize($_SESSION["usr"]);
    $user->inheritPDO($GLOBALS["app"]->shareDB());
    $var = $user->editProfileUser($email, $pswd, $gender, $date, $ico, $dic, $company);
    if($var == "done"){
      unset($_SESSION["succ"]);
      unset($_SESSION["usr"]);
      $_SESSION["usr"] = serialize($user);
      $_SESSION["succ"] = "Váš profil byl úspěšně upraven";
      header("Location: pages/profile.php");
      die();
    }else{
      error($var);
    }
  }

  //[ADMIN]  Funkce pro smazání uživatele
  function deleteUser($id){
    $user = unserialize($_SESSION["usr"]);
    $user->inheritPDO($GLOBALS["app"]->shareDB());
    $response = $user->deleteUser($id);
    if($response == "done"){
      header("Location: pages/users.php");
      die();
    }else{
      error($response);
    }
  }

  function newArticle($uid, $name, $content, $target, $status){
    $response = $GLOBALS["app"]->newArticle($uid, $name, $content, $target, $status);
    echo $response;
   if($response == "done"){
      unset($_SESSION["succ"]);
      $_SESSION["succ"] = "Váš příspěvek byl úspěšně vytvořen";
      header("Location: http://".URLINDEX."/admin/pages/articles.php");
      die();
    }else{
      error($response);
    }
  }

//Funkce pro vrácení chybné hlášky neexistující operace
  function not_existing_action($action){
    unset($_session["error"]);
    $_session["error"] = "Požadovaná akce ". $action ." neexistuje!";
    header("Location: http://".URLINDEX."/index.php");
    die();
  }

  function error($pm){
    unset($_SESSION["error"]);
    $_SESSION["error"] = $pm;
    header("Location: http://".URLINDEX."/index.php");
    die();
  }

/* ******************************* GET FUNKCE/OPERACE *************************** */
  function logout(){
    unset($_SESSION["usr"]);
    unset($_SESSION["succ"]);
    $_SESSION["succ"] = "Byl jste úspešně odhlášen";
    header("Location: ../index.php");
    die();
  }

  function deleteArticle($uid, $aid){
    $response = $GLOBALS["app"]->deleteArticle($uid, $aid);
    if($response == "done"){
      unset($_SESSION["succ"]);
      $_SESSION["succ"] = "Váš příspěvek byl úspěšně smazán";
      header("Location: http://".URLINDEX."/admin/pages/articles.php");
      die();
    }else{
      error($response);
    }
  }

 ?>
