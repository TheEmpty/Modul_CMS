<ul id="slide-out" class="sidenav">
  <li>
    <div class="user-view">
      <div class="background">
        <img src= "<?php echo 'http://'. URLINDEX .'/admin/images/default_background.jpg'; ?>">
      </div>
      <a href='<?php echo "http://".URLINDEX."/admin/pages/profile.php" ?>'><img class="circle" src="<?php echo "http://". URLINDEX."/admin/".$user->show("image") ?>"></a>
      <a href='<?php echo "http://".URLINDEX."/admin/pages/profile.php" ?>'><span class="black-text name"> <?php echo $user->show("fname") ." ". $user->show("sname");  ?></span></a>
      <span class="black-text email"><?php echo $user->show("email"); ?></span>
    </div>
  </li>
  <?php
  if($user->show("level") == 10){ //user level sidenav
    echo "
    <li> <a href='http://".URLINDEX."/admin/index.php'><i class='material-icons'> explore </i> Úvodní stránka </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/profile.php'><i class='material-icons'> person </i> Profil </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/dashboard.php'><i class='material-icons'> insert_chart </i> Dashboard </a> </li>
    <li> <a href='http://".URLINDEX."/admin/core.php?action=logout' class='waves-effect'> <i class='material-icons'> power_off </i> Odhlásit se </a> </li>
    ";
  }elseif($user->show("level") == 1){ //admin level sidenav
    echo "
    <li> <a href='http://".URLINDEX."/admin/index.php'><i class='material-icons'> explore </i> Úvodní stránka </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/profile.php'><i class='material-icons'> person </i> Profil </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/dashboard.php'><i class='material-icons'> insert_chart </i> Dashboard </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/articles.php'><i class='material-icons'> view_day </i> Příspěvky </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/moduls.php'><i class='material-icons'> flip_to_back </i> Moduly </a> </li>
  <!--  <li> <a href='http://".URLINDEX."/admin/pages/new_article.php'><i class='material-icons'> note_add </i> Nový příspěvek </a> </li> -->
    <li> <a href='http://".URLINDEX."/admin/pages/users.php'><i class='material-icons'> person </i> Uživatelé </a> </li>
    <li> <a href='http://".URLINDEX."/admin/pages/moduls.php'><i class='material-icons'> view_quilt </i> Moduly </a> </li>
    <li> <a href='http://".URLINDEX."/admin/core.php?action=logout' class='waves-effect'> <i class='material-icons'> power_off </i> Odhlásit se </a> </li>
    ";
  }
    ?>
  <li><div class="divider"></div></li>
  <li><a class="subheader"> Přístupné moduly </a></li>
  <?php
    foreach($_SESSION["moduls_array"] as $m_a){
      if(!empty($m_a["vis"])){
        continue;
      }
      $v = MODUL_PATH_FOR_EXCLUDE;
      $path = explode($v ,$m_a["path"]);
      echo '<li> <a href="http://'.URLINDEX.$path[1].'"> <i class="material-icons"> '.$m_a["icon"].' </i> '.$m_a["name"] . $m_a["error"].' </a> </li>';
    }
    ?>
</ul>
<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons large">arrow_right</i></a>
