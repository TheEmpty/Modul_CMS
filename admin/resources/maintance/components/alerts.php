<?php
/* *************** Hlášení výsledku operací (Toasty) ******************************* */
  if(!empty($_SESSION["succ"])){
    echo "<script> $(document).ready(function(){ M.toast({html: '".$_SESSION["succ"]."'}) });</script>";
    unset($_SESSION["succ"]);
  }

  if(!empty($_SESSION["error"])){
    echo "<script> $(document).ready(function(){ M.toast({html: '". $_SESSION["error"] ."', classes: 'red'}) }); </script>";
    unset($_SESSION["error"]);
  }

 ?>
