<h1 class="center"> Přihlášení uživatele </h1>
<div class="row">
  <form class="col s9 offset-s4" action="admin/core.php" method="post">
    <div class="row">
      <div class="input-field col s6">
        <input type="email" id="email" name="email" class="validate"/>
        <label for="email" data-error="Špatný formát emailu!"> Email </label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input type="password" id="pswd" name="pswd" class="validate" minlength="5"/>
        <label for="pswd" data-error="Pole musí obsahovat minimálně 5 symbolů."> Heslo </label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <button class="btn waves-effect waves-light right" type="submit" name="action" value="login"> Přihlásit se </button>
      </div>
    </div>
    <div class="row">
      <p> Ještě nemám účet, potřebuji se <a class="reg_btn" href="#"> registrovat </a> </p>
    </div>
  </form>
</div>
