<h1 class="center"> Registrace uživatele </h1>
<div class="row">
  <form class="col s9 offset-s4" action="admin/core.php" method="post">
    <div class="row">
      <div class="input-field col s3">
        <input type="text" id="firstname" name="firstname" class="validate" minlength="3"/>
        <label for="firstname" data-error="Pole musí obsahovat alespoň 3 symboly."> Křestní jméno </label>
      </div>
      <div class="input-field col s3">
        <input type="text" id="surname" name="surname" class="validate" minlength="3"/>
        <label for="surname" data-error="Pole musí obsahovat minimálně 3 symboly."> Příjmení </label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input type="email" id="email" name="email" class="validate"/>
        <label for="email" data-error="Nesprávný formát emailu."> Email </label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input type="password" id="pswd" name="pswd" class="validate" minlength="5"/>
        <label for="pswd" data-error="Pole musí obsahovat minimálně 5 symbolů."> Heslo </label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input type="hidden" name="level" value="1"/>
        <button class="btn waves-effect waves-light right" type="submit" name="action" value="registrate"> Zaregistrovat se </button>
      </div>
    </div>
    <div class="row">
      <p> Už mám účet, chtěl bych se <a class="login_btn" href="#"> přihlásit </a> </p>
    </div>
  </form>
</div>
