<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Dashboard | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.sidenav').sidenav();
          $('.modal').modal();
          $('.datepicker').datepicker();
          $('select').formSelect();
        });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "../resources/maintance/components/sidenav.php";
        require_once "../resources/maintance/components/alerts.php";
       ?>
       <h2 class="center"> Dashboard </h2>
       <div class="row">
         <div class="col s12">
           <table class="responsive_table striped col s6 offset-s3">
            <?php
            $users = $app->countUsers();
              echo
              '
                <tr>
                  <td> Počet modulů: </td>
                  <td> '. $app->countModuls()[0][0] .' </td>
                </tr>
                <tr>
                  <td> Počet příspěvků: </td>
                  <td> '. $app->countArticles()[0][0] .' </td>
                </tr>
                <tr>
                  <td> Počet registrovaných uživatelů: </td>
                  <td> '. $users['all'][0] .' </td>
                </tr>
                <tr>
                  <td> Počet ověřených uživatelů: </td>
                  <td> '. $users['verified'][0] .' </td>
                </tr>
              ';
            ?>
           </table>
         </div>
       </div>
    </div>
  </body>
</html>
