<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Dashboard | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          M.AutoInit();
        });
    </script>
  </head>
  <body>
    <div class="col s12">
      <?php
        require_once "../resources/maintance/components/sidenav.php";
        require_once "../resources/maintance/components/alerts.php";
      ?>
      <div class="row">
        <div class="col s12">
          <h1 class="center"> Správa příspěvků </h1>
          <div class="col s10 offset-s1">
            <div class="row">
              <a href="new_article.php" class="btn tooltipped right" data-position="bottom" data-tooltip="Vytvořit nový příspěvek"> Nový příspěvek </a>
            </div>
            <table class="responsive_table striped">
              <tr>
                <td> Číslo příspěvku </td>
                <td> Název příspěvku </td>
                <td> Autor příspěvku </td>
                <td> Cílová úroveň </td>
                <td> Status příspěvku </td>
                <td> Operace </td>
              </tr>
              <?php
              $allArticles = $app->showAllArticles();
              foreach($allArticles as $aa){
                  echo
                  '
                    <tr>
                      <td> '. $aa["ID"] .' </td>
                      <td> '. $aa["Name"] .' </td>
                      <td> '. $aa["Firstname"] .' '.$aa["Surname"].' </td>
                      <td> '. $aa["Target"] .' </td>
                      <td> '. $aa["Status"] .' </td>
                      <td>
                        <a href="../core.php?action=delArt&uid='.$user->show("id").'&aid='.$aa["ID"].'" class="btn tooltipped red" data-position="bottom" data-tooltip="Smazat příspěvek"> <i class="material-icons"> delete_forever</i></a>
                      </td>
                    </tr>
                  ';
                }
               ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
