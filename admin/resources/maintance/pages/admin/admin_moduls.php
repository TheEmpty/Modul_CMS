<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Admin | Moduly | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.sidenav').sidenav();
          $('.modal').modal();
          $('.datepicker').datepicker();
          $('select').formSelect();
        });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "../resources/maintance/components/sidenav.php";
        require_once "../resources/maintance/components/alerts.php";
       ?>
       <h2 class="center"> Výpis modulů </h2>
       <div class="row">
         <div class="col s12">
           <div class="col s10 offset-s1">
             <table class="striped responsive_table">
               <tr>
                 <td> Modul </td>
                 <td> Název modulu </td>
                 <td> Popisek modulu </td>
                 <td> Autor modulu </td>
                 <td> Zobrazení modulu </td>
               </tr>
               <?php
                  $regModuls = $app->showModuls();
                  foreach($regModuls as $mods){
                    if($mods["Visibility"] == 1){
                      $vis = "Povoleno";
                    }elseif($mods["Visibility"] == 0){
                      $vis = "Zakázáno";
                    }else{
                      $vis = "Nedefinovaná hodnota";
                    }
                  echo '
                    <tr>
                      <td> '. $mods["Name"] .'</td>
                      <td> '. $mods["Nickname"] .'</td>
                      <td> '. $mods["Description"] .'</td>
                      <td> '. $mods["Author"] .'</td>
                      <td> '. $vis .'</td>
                    </tr>
                  ';
                  }
                ?>
             </table>
           </div>
         </div>
       </div>
    </div>
  </body>
</html>
