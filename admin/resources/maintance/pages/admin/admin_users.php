<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Dashboard | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          M.AutoInit();
        });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
      require_once "../resources/maintance/components/sidenav.php";
      require_once "../resources/maintance/components/alerts.php";
      ?>
      <div id="newUserModalWindow" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h2> Vytvořit nového uživatele </h2>
          <form class="col s12" action="../core.php" method="post">
            <div class="row">
              <div class="input-field col s6">
                <input type="text" id="firstname" name="firstname" class="validate" minlength="3"/>
                <label for="firstname" data-error="Pole musí obsahovat alespoň 3 symboly."> Křestní jméno </label>
              </div>
              <div class="input-field col s6">
                <input type="text" id="surname" name="surname" class="validate" minlength="3"/>
                <label for="surname" data-error="Pole musí obsahovat minimálně 3 symboly."> Příjmení </label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s10">
                <input type="email" id="email" name="email" class="validate"/>
                <label for="email" data-error="Nesprávný formát emailu."> Email </label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s10">
                <input type="password" id="pswd" name="pswd" class="validate" minlength="5"/>
                <label for="pswd" data-error="Pole musí obsahovat minimálně 5 symbolů."> Heslo </label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s10">
                <input type="hidden" name="level" value="1"/>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn waves-effect waves-light right" type="submit" name="action" value="registrate"> Vytvořit nový profil </button>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Zrušit</a>
          </div>
        </form>
      </div>
      <h1 class="center"> Správa uživatelů </h1>
      <div class="row">
        <div class="col s10 offset-s1">
          <a href="#newUserModalWindow" class="btn right modal-trigger">Nový uživatel</a>
        </div>
      </div>
      <div class="row">
        <div class="col s10 offset-s1">
          <table class="striped responsive_table">
            <tr class="grey">
              <td> Jméno uživatele </td>
              <td> Email </td>
              <td> ICO </td>
              <td> Company </td>
              <td> Oprávnění </td>
              <td> Ověřeno </td>
              <td class="brown"> Operace </td>
            </tr>
            <?php
              $allUs = $user->showUsers();
              foreach($allUs as $us){
                echo '
                  <tr>
                    <td> '. $us["Firstname"] .' '. $us["Surname"] .' </td>
                    <td> '. $us["Email"] .' </td>
                    <td> '. $us["ICO"] .' </td>
                    <td> '. $us["Company"] .' </td>
                    <td> '. $us["Level"] .' </td>
                    <td> '. $us["Verified"] .'</td>
                    <td>
                      <form action="../core.php" method="POST">
                        <input name="uid" type="hidden" value="'.$us["ID"].'"/>
                        <a href="#" class="btn tooltipped" data-position="bottom" data-tooltip="Detaily" title="Vytvořit tooltip pro detaily!"> <i class="material-icons"> notes </i> </a>
                        <a href="#userEditModal" class="btn tooltipped modal-trigger editUser" data-position="bottom" data-tooltip="Editovat" title="Vytvořit tooltip pro editovat!"> <i class="material-icons"> edit </i> </a>
                        <button class="btn red tooltipped" data-position="bottom" data-tooltip="Smazat" title="Vytvořit tooltip pro smazat!" type="submit" name="action" value="deleteUser"> <i class="material-icons"> delete_forever </i> </button>
                      </form>
                    </td>
                  </tr>
                ';
              }
            ?>
          </table>
        </div>
      </div>
      <?php
      echo
      '
      <!-- Editace uživatelského profilu *** MODAL OKNO *** -->
      <div id="userEditModal" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h2> Upravit profil </h2>
          <div class="row">
            <form class="col s8" action="../core.php" method="POST">
              <div class="row">
                <div class="input-field col s12">
                    <input type="email" name="email" id="email" value=""/>
                    <label for="email"> Email: </label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <select name="gender">
                    <option value="" disabled selected> Vyberte své pohlaví </option>
                    <option value="Muž"> Muž </option>
                    <option value="Žena"> Žena </option>
                    <option value="Jiné"> Jiné </option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                  <input type="text" id="birth" name="birth" class="datepicker" placeholder="Datum narození: "/>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  <input type="number" name="ico" id="ico" value="'.$us.'"/>
                  <label for="ico"> IČO: </label>
                </div>
                <div class="input-field col s6">
                  <input type="number" name="dic" id="dic" value="'.$us.'"/>
                  <label for="dic"> DIČ: </label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input type="text" name="company" id="company" value="'.$us.'"/>
                  <label for="company"> Společnost: </label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn waves-effect waves-light right" type="submit" name="action" value="editProfileUser"> Upravit profil</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Zrušit</a>
          </div>
        </form>
      </div>
      ';
       ?>
    </div>
  </body>
</html>
