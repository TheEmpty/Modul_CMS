<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Dashboard | Modul_cms </title>
    <script type="text/javascript">
        $(document).ready(function(){
          $('.sidenav').sidenav();
          $('.modal').modal();
          $('.datepicker').datepicker();
          $('select').formSelect();
        });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "../resources/maintance/components/sidenav.php";
        require_once "../resources/maintance/components/alerts.php";
       ?>
    </div>
  </body>
</html>
