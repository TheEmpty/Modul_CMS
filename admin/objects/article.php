<?php

  class ARTICLE
  {

    protected $db;
    protected $id;
    protected $aid;
    private $name;
    private $description;
    private $content;
    private $target;
    private $status;

    function __construct($db, $a){
      $this->db = $db;
      $this->id = $a["ID"];
      $this->aid = $a["AID"];
      $this->name = $a["Name"];
      $this->description = $a["Description"];
      $this->content = $a["Content"];
      $this->target = $a["TargetGroup"];
      $this->status = $a["Status"];
    }

    public function show($el){
      return $this->$el;
    }


  }



 ?>
