<?php

  class APP
  {

    protected $db;

    //Konstruktor objektu
    function __construct($DB_conn)
    {
      $this->db = $DB_conn;
    }

    public function shareDB(){
      return $this->db;
    }

    public function login_user($email, $password){
      try {
        $pswd = hash("sha256", $password);
        $s_sql = $this->db->prepare("SELECT u.ID, u.Firstname, u.Surname, u.Email, u.Level, m.Image, m.Birth, m.Gender, m.ICO, m.DIC, m.Company FROM m_users AS u JOIN m_usersmeta AS m ON u.ID = m.UID WHERE Email = :email AND Password = :pswd LIMIT 1");
        $s_sql->execute(array(":email" => $email, ":pswd" => $pswd));
        $r_sql = $s_sql->fetch(PDO::FETCH_ASSOC);
        if(!empty($r_sql)){
          unset($_SESSION["usr"]);
          require_once "user.php";
          $usr = new USER($this->db, $r_sql);
          return $_SESSION["usr"] = serialize($usr); //přenesení instance
        }else{
          return "wrong";
        }
      } catch (PDOException $e) {
        echo $e->getMessage();
      }
    }

    public function registrate_user($fname, $sname, $email, $password, $lvl){
      try {
        $pswd = hash("sha256", $password);
        $reg_time = date("Y-m-d h:i");
        $s_sql = $this->db->prepare("SELECT ID FROM m_users WHERE Email = :email LIMIT 1");
        $s_sql->execute(array(":email" => $email));
        $r_sql = $s_sql->fetch(PDO::FETCH_ASSOC);
        if(empty($r_sql)){
          $stmt = $this->db->prepare("INSERT INTO m_users(Firstname, Surname, Email, Password, Level) VALUES (:fname, :sname, :email, :pswd, :level)");
          $stmt->execute(array(":fname" => $fname, ":sname" => $sname, ":email" => $email, ":pswd" => $pswd, ":level" => $lvl));
          $uid = $this->db->lastInsertId();
          $stmtm = $this->db->prepare("INSERT INTO m_usersmeta(UID, Image, Created, Verified) VALUES (:uid, :image, :created, :verified)");
          $stmtm->execute(array(":uid" => $uid, ':image' => '', ':created' => $reg_time, ':verified' => 'Neověřeno'));
          return "done";
        }else{
          return "error";
        }
      } catch (PDOException $e) {
        echo $e->getMessage();
      }

    }

    public function showModuls(){
      try{
        $sql = $this->db->prepare("SELECT * FROM m_moduls");
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function countModuls(){
      try{
        $sql = $this->db->prepare("SELECT COUNT(*) FROM m_moduls");
        $sql->execute();
        $result = $sql->fetchAll();
        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function countArticles(){
      try{
        $sql = $this->db->prepare("SELECT COUNT(*) FROM m_articles");
        $sql->execute();
        $result = $sql->fetchAll();
        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function countUsers(){
      try{
        $sql = $this->db->prepare("SELECT COUNT(*) FROM m_users");
        $sql->execute();
        $all = $sql->fetchAll();
        $sq = $this->db->prepare("SELECT COUNT(*) FROM m_usersmeta WHERE Verified = :ver");
        $sq->execute(array(":ver" => "Ověřeno"));
        $unver = $sq->fetchAll();
        $result = array(
          "all" => $all[0],
          "verified" => $unver[0],
        );

        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function newArticle($aid, $name, $content, $target, $status){
      try{
        $created = date("Y-m-d h:i");
        $sql = $this->db->prepare("INSERT INTO m_articles(AID, Name, Content, Target, Status, Created) VALUES (:aid, :name, :content, :target, :status, :create)");
        $sql->execute(array(":aid" => $aid, ":name" => $name, ":content" => $content, ":target" => $target, ":status" => $status, ":create" => $created));
        unset($_SESSION["succ"]);
        $_SESSION["succ"] = "Příspěvek úspěšně vytvořen";
        return "done";
      }catch(PDOException $e){
        unset($_SESSION["err"]);
        $_SESSION["err"] = "Nastala chyba při vytvoření příspěvku";
      }
    }

    public function showArticles($par){
      try{
        $sql = $this->db->prepare("SELECT * FROM m_articles WHERE Status = :stat");
        $sql->execute(array(":stat" => $par));
        $result = $sql->fetchAll();
        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function showAllArticles(){
      try{
        $sql = $this->db->prepare("SELECT a.ID, a.Name, a.Description, a.Content, a.Created, a.Status, a.Target, u.Firstname, u.Surname FROM m_articles AS a JOIN m_users AS u ON a.AID = u.ID");
        $sql->execute();
        $result = $sql->fetchAll();
        return $result;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    public function deleteArticle($uid, $aid){
      try {
        $sql = $this->db->prepare("DELETE FROM m_articles WHERE ID = :id LIMIT 1");
        $sql->execute(array(":id" => $aid));
        return "done";
      } catch (PDOException $e) {
        return $e->getMessage();
      }

    }

}
 ?>
