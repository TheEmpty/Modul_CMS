<?php

  class MODUL
  {

    private $db;
    private $id;
    private $name;
    private $description;
    private $version;
    private $version_name;
    private $state;
    private $author;
    private $path;
    private $menu_v;
    private $menu_n;
    private $menu_i;

    function __construct($d, $db){

      $this->db = $db;
      $this->name = $d["name"];
      $this->description = $d["description"];
      $this->version = $d["version"];
      $this->path = $d["path"];
      $this->menu_v = $d["menu_v"];
      $this->menu_n = $d["menu_n"];
      $this->menu_i = $d["menu_i"];

      if(empty($d["id"]) OR ($d["id"] == "0")){
        try
        {
          $sql = $this->db->prepare("SELECT ID, Name, Location FROM m_moduls WHERE Name = :name AND Location = :loc LIMIT 1");
          $sql->execute(array(":name" => $d["name"], ":loc" => $d["path"]));
          if($sql->rowCount() > 0){
            $result = $sql->fetch(PDO::FETCH_ASSOC);
            $this->id = $result["ID"];
          }else{
            $sql_w = $this->db->prepare("INSERT INTO m_moduls(Name, Nickname, Description, Location, Visibility, Author) VALUES (:name, :nick, :descr, :loc, :vis, :author)");
            $sql_w->execute(array(":name" => $d["name"], ":nick" => $d["menu_n"], ":descr" => $d["description"], ":loc" => $d["path"], ":vis" => $d["menu_v"], ":author" => $d["author"]));
            $this->id = $this->db->lastInsertId();
          }
        }catch(PDOException $e){
          echo $e->getMessage();
        }
      }

    }

    public function show($el){
      return $this->$el;
    }

    public function __sleep(){
      return array('id', 'name', 'description', 'version', 'version_name', 'state', 'author', 'path', 'menu_v', 'menu_n', 'menu_i');
    }

    public function inheritPDO($db){
      $this->db = $db;
    }

    public function addInMenu(){
      if(!empty($this->menu_v) OR $this->menu_v != 0){
        if(!empty($this->path)){
          if(!empty($this->menu_n)){
            if(!empty($this->menu_i)){
              return array(
                'id' => $this->id,
                'name' => $this->menu_n,
                'icon' => $this->menu_i,
                'path' => $this->path,
              );
            }else{
              return array(
                'id' => $this->id,
                'name' => $this->menu_n,
                'icon' => 'fiber_new',
                'path' => $this->path,
              );
            }
          }else {
            return array(
              'error' => 'Název není definován!',
              'icon' => 'error',
            );
          }
        }else{
          return array(
            'error' => 'Není definována cesta k modulu!',
            'icon' => 'error',
          );
        }
      } else {
      return array(
        'vis' => 'Modul nemá povolenou viditelnost',
      );
    }
  }

}

 ?>
