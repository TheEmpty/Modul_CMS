<?php

  class USER
  {

    private $db;
    protected $id;
    private $fname;
    private $sname;
    protected $email;
    private $level;
    //Metadata
    private $image;
    private $birth;
    private $gender;
    private $ico;
    private $dic;
    private $company;

    function __construct($Db, $data)
    {
      $this->db = $Db;
      $this->id = $data["ID"];
      $this->fname = $data["Firstname"];
      $this->sname = $data["Surname"];
      $this->email = $data["Email"];
      $this->level = $data["Level"];
      if(empty($data["Image"]) OR $data["Image"] == "null"){
        $this->image = "images/default_user.jpg";
      }
      $this->birth = $data["Birth"];
      $this->gender = $data["Gender"];
      $this->ico = $data["ICO"];
      $this->dic = $data["DIC"];
      $this->company = $data["Company"];
    }

    public function show($el){
      return $this->$el;
    }

    public function inheritPDO($db){
      $this->db = $db;
      return "done";
    }

    public function logout(){
      unset($_SESSION["usr"]);
      return "done";
    }

    public function editProfileUser($email, $pswd, $gender, $date, $ico, $dic, $company){
      if($pswd != ""){
        $pswd = hash("sha256", $pswd);
      }
      $born = strtotime($birth);
      $birth = date('Y-m-d', $born);

      $sql = $this->db->prepare("UPDATE m_users SET Email = COALESCE(NULLIF(:mail, ''), Email), Password = COALESCE(NULLIF(:pswd, ''), Password) WHERE ID = :id LIMIT 1");
      $sql->execute(array(":mail" => $email, ":pswd" => $pswd, ':id' => $this->id));

      $sqlm = $this->db->prepare("UPDATE m_usersmeta SET Image = COALESCE(NULLIF(:image, ''), Image), Birth = COALESCE(NULLIF(:birth, ''), Birth), Gender = COALESCE(NULLIF(:gender, ''), Gender), ICO = COALESCE(NULLIF(:ico, ''), ICO), DIC = COALESCE(NULLIF(:dic, ''), DIC), Company = COALESCE(NULLIF(:company, ''), Company) WHERE UID = :uid LIMIT 1");
      $sqlm->execute(array(":image" => $image, ':birth' => $birth, ':gender' => $gender, ':ico' => $ico, ':dic' => $dic, ':company' => $company, ':uid' => $this->id));

      $this->syncWithDb();
      return "done";
    }

    public function __sleep(){
      return array('id', 'fname', 'sname', 'email', 'level', 'image', 'birth', 'gender', 'ico', 'dic', 'company');
    }

    private function syncWithDb(){
      $sql = $this->db->prepare("SELECT u.Firstname, u.Surname, u.Email, u.Level, m.Image, m.Birth, m.Gender, m.ICO, m.DIC, m.Company, m.Created, m.Verified FROM m_users AS u JOIN m_usersmeta AS m ON u.ID = m.UID WHERE u.ID = :id LIMIT 1");
      $sql->execute(array(':id' => $this->id));
      $result = $sql->fetch(PDO::FETCH_ASSOC);

      $this->fname = $result["Firstname"];
      $this->sname = $result["Surname"];
      $this->email = $result["Email"];
      $this->level = $result["Level"];
      if(empty($result["Image"]) OR $result["Image"] == null){
        $this->image = "images/default_user.jpg";
      }else{
        $this->image = $result["Image"];
      }
      $this->birth = $result["Birth"];
      $this->ico = $result["ICO"];
      $this->dic = $result["DIC"];
      $this->gender = $result["Gender"];
      $this->company = $result["Company"];

      return "done";
    }

    public function showUsers(){
      $sql = $this->db->prepare("SELECT u.ID, u.Firstname, u.Surname, u.Email, u.Level, m.Birth, m.Gender, m.ICO, m.DIC, m.Company, m.Created, m.Verified FROM m_users AS u JOIN m_usersmeta AS m ON u.ID = m.UID");
      $sql->execute();
      $result = $sql->fetchAll();
      return $result;
    }

    public function deleteUser($uid){
      if($this->level == 1){
        try {
        //  $sql = $this->db->prepare(("DELETE FROM m_users WHERE ID = :id UNION DELETE FROM m_usersmeta WHERE UID = :id"));
          $sql = $this->db->prepare("DELETE FROM m_users WHERE ID = :id LIMIT 1");
          $sql->execute(array(":id" => $uid));
          $sql2 = $this->db->prepare("DELETE FROM m_usersmeta WHERE UID = :id LIMIT 1");
          $sql2->execute(array(":id" => $uid));
          $_SESSION["succ"] = "Profil byl úspěšně odstraněn";
          return "done";
        } catch (PDOException $e) {
          return $e->getMessage();
        }
      }else{
        unset($_SESSION["error"]);
        $_SESSION["error"] = "Nemáte dostatečná oprávnění pro tuto operaci";
        return "Nemáte dostatečná oprávnění";
      }
    }

    public function getMyLiveArticleFeed(){
      if(!empty($this->company)){ //PŘEDĚLAT PRO JEDNOTLIVÉ SPOLEČNOSTI!
        try {
          if($this->level == "1"){ //Admin level ONLY (lvl 10)
            $sql = $this->db->prepare("SELECT a.ID, a.Name, a.Description, a.Content, a.Target, a.Created, u.Firstname, u.Surname FROM m_articles AS a JOIN m_users AS u ON a.AID = u.ID WHERE a.Target = :target AND a.Status = :status ORDER BY a.ID DESC");
            $sql->execute(array(":target" => "Administrátoři", ":status" => "Publikováno"));
          }elseif($this->level == "10"){ //Users / users-admin
            $sql = $this->db->prepare("SELECT a.ID, a.Name, a.Description, a.Content, a.Target, a.Created, u.Firstname, u.Surname FROM m_articles AS a JOIN m_users AS u ON a.AID = u.ID WHERE a.Target = :target AND a.Status = :status ORDER BY a.ID DESC");
            $sql->execute(array(":target" => $this->company, ":status" => "Publikováno"));
          }
          $result = $sql->fetchAll();
          return $result;
        } catch (PDOException $e) {
          return $e->getMessage();
        }

      }else{
        try {
          if($this->level == "1"){ //Admin level ONLY (lvl 10)
            $sql = $this->db->prepare("SELECT a.ID, a.Name, a.Description, a.Content, a.Target, a.Created, u.Firstname, u.Surname FROM m_articles AS a JOIN m_users AS u ON a.AID = u.ID WHERE a.Target = :target AND a.Status = :status ORDER BY a.ID DESC");
            $sql->execute(array(":target" => "Administrátoři", ":status" => "Publikováno"));
          }elseif($this->level == "10"){ //Users / users-admin
            $sql = $this->db->prepare("SELECT a.ID, a.Name, a.Description, a.Content, a.Target, a.Created, u.Firstname, u.Surname FROM m_articles AS a JOIN m_users AS u ON a.AID = u.ID WHERE a.Target = :target AND a.Status = :status ORDER BY a.ID DESC");
            $sql->execute(array(":target" => $this->company, ":status" => "Publikováno"));
          }
          $result = $sql->fetchAll();
          return $result;
        } catch (PDOException $e) {
          return $e->getMessage();
        }
      }
    }

  }

 ?>
