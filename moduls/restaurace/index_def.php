<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../../admin/config.php";

  //PHP Mailer


  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require_once "../../vendor/autoload.php";

/*  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  try {
      //Server settings
      $mail->SMTPDebug = 2;                                 // Enable verbose debug output
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;
      $mail->isHTML(true);                        // Enable SMTP authentication
      $mail->Username = 'testerkocvara@gmail.com';                 // SMTP username
      $mail->Password = 'shipuden285';                           // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      //Recipients
      $mail->setFrom('testerkocvara@gmail.com', 'John');
      $mail->addAddress('kocvara.jan.it@gmail.com', 'Jan Kočvara');     // Add a recipient
    //  $mail->addAddress('ellen@example.com');               // Name is optional
  //    $mail->addReplyTo('info@example.com', 'Information');
    //  $mail->addCC('cc@example.com');
    //  $mail->addBCC('bcc@example.com');

      //Attachments
    //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

      //Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = 'This is a test email';

      $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
      $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      $mail->send();
      echo 'Message has been sent';
  } catch (Exception $e) {
      echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
  }*/

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);
?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../../admin/resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="library/html2canvas.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="css/recipe.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Administrace | M_CMS </title>
    <script type="text/javascript">
      $(document).ready(function(){
        M.AutoInit();
        var lastCategory = 0;
        var lastItem = 0;

        $(document.body).on('click', '.new_category', function(e){
          lastCategory = lastCategory + 1;
          $('.recipe').append('<div class="row"> <div class="category red lighten-5 category_'+ lastCategory +'"> <div class="red lighten-5 input-field col s11"> <input type="text" id="title_'+lastCategory+' name="title_'+lastCategory+'/> <label for="title_'+lastCategory+'">Nadpis kategorie</label> </div> <a class="btn new_item" value="'+lastCategory+'"><i class="material-icons"> add </i> </a> </div> </div>');
        });

        $(document.body).on('click', '.new_item', function(e){
          let category_number = jQuery(this).attr('value');
          lastItem = lastItem + 1;
          $('.category_'+category_number).append('<div class="items"> <div class="col s8 offset-s1"> <input id="item_'+ lastItem +'" type="text" name="item_'+ lastItem +'"/> <label for="item_'+ lastItem +'"> Položka číslo '+ lastItem +'</label> </div> <div class="col s1"> <input id="price_"'+lastItem+'" type="number" name="price_'+lastItem+'"/> </div> </div>');
      });

      var element = $("#html-content-holder"); // global variable
      var getCanvas; // global variable

      $("#btn-Preview-Image").on('click', function () {
        $('input').each(function(){
            let v = $(this).val();
            $(this).replaceWith("<p style='font-size: 25; font-family: arial;'>"+ v +"</p>");
            $('label').remove();
            $('.new_item').remove();
            $('.new_category').remove();
          });
           html2canvas(element, {
           onrendered: function (canvas) {
                  $("#previewImage").append(canvas);
                  getCanvas = canvas;
               }
           });
      });

      $("#btn-Convert-Html2Image").on('click', function () {
        var imgageData = getCanvas.toDataURL("image/png");
        // Now browser starts downloading it instead of just showing it
        var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
        let actualDate = "jidelnicek_<?php echo date('d-m-Y'); ?>";
        $("#btn-Convert-Html2Image").attr("download", actualDate+".png").attr("href", newData);
      });
    });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "../../admin/resources/maintance/components/sidenav.php";
       ?>
      <div class="row">
        <div class="col s10">
          <button class="btn new_category" type="button" name="button"> New Category </button>
          <form id="html-content-holder" class="col s7 offset-s1 recipe white" action="#" method="post">
            <div class="row">
              <div class="col s6 offset-s3">
                <input id="main_title" type="text" name="main_title"/>
                <label for="main_title"> Hlavička jídelníčku </label>
              </div>
            </div>
          </form>
        </div>
        <div class="col s2">
          <div class="col s12">
            <button id="btn-Preview-Image" class="btn" value="Preview"> Připravit ke stažení </button>
            <a href="#!" id="btn-Convert-Html2Image" class="waves-effect waves-green btn-flat"> <button class="btn"> Stáhnout </button> </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s5">
          <div class="inpupt-field col s12">
          <!--  <select multiple class="validate" name="target">
              <option value="1"> Haf </option>
              <option value="2"> Mnau </option>
            </select>-->
            <input id="email" type="email" name="email" placeholder="nekdo@nekdo.cz"/>
            <label for="email"> Komu to pošleme? </label>
          </div>
        </div>
      </div>
    </div>
    <?php
      require_once "../../admin/resources/maintance/components/alerts.php";
     ?>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="../../admin/resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
