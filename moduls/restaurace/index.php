<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../../admin/config.php";

  //PHP Mailer


  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require_once "../../vendor/autoload.php";

/*  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  try {
      //Server settings
      $mail->SMTPDebug = 2;                                 // Enable verbose debug output
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;
      $mail->isHTML(true);                        // Enable SMTP authentication
      $mail->Username = 'testerkocvara@gmail.com';                 // SMTP username
      $mail->Password = 'shipuden285';                           // SMTP password
      $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 465;                                    // TCP port to connect to

      //Recipients
      $mail->setFrom('testerkocvara@gmail.com', 'John');
      $mail->addAddress('kocvara.jan.it@gmail.com', 'Jan Kočvara');     // Add a recipient
    //  $mail->addAddress('ellen@example.com');               // Name is optional
  //    $mail->addReplyTo('info@example.com', 'Information');
    //  $mail->addCC('cc@example.com');
    //  $mail->addBCC('bcc@example.com');

      //Attachments
    //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

      //Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = 'This is a test email';

      $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
      $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      $mail->send();
      echo 'Message has been sent';
  } catch (Exception $e) {
      echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
  }*/

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);
?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../../admin/resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/recipe.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Administrace | M_CMS </title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/html2canvas.js"></script>
    <script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        M.AutoInit();
        var lastCategory = 0;
        var lastItem = 0;

        $(document.body).on('click', '.new_category', function(e){
          lastCategory = lastCategory + 1;
          $('.recipe').append('<div class="row"> <div class="category red lighten-5 category_'+ lastCategory +'"> <div class="red lighten-5 input-field col s11"> <input class="input" type="text" id="title_'+lastCategory+' name="title_'+lastCategory+'/> <label for="title_'+lastCategory+'">Nadpis kategorie</label> </div> <a class="btn new_item tooltipped" data-position="right" data-tooltip="Vytvořit položku" value="'+lastCategory+'"><i class="material-icons"> add </i> </a> </div> </div>');
        });

        $(document.body).on('click', '.new_item', function(e){
          let category_number = jQuery(this).attr('value');
          lastItem = lastItem + 1;
          $('.category_'+category_number).append('<div class="items"> <div class="col s8 offset-s1"> <input class="input" id="item_'+ lastItem +'" type="text" name="item_'+ lastItem +'"/> <label for="item_'+ lastItem +'"> Položka číslo '+ lastItem +'</label> </div> <div class="col s1"> <input id="price_"'+lastItem+'" type="number" name="price_'+lastItem+'"/> </div> </div>');
      });
    });
    </script>
  </head>
  <body>
    <div class="page col s12">
      <?php
        require_once "../../admin/resources/maintance/components/sidenav.php";
       ?>
      <div class="row">
        <div class="col s8 offset-s1">
          <button class="btn new_category" type="button" name="button"> New Category </button>
          <form id="myForm" enctype="multipart/form-data" class="col s7 offset-s1 white" action="save.php" method="post">
            <input type="hidden" name="img_val" id="img_val" value="" />
            <input type="hidden" name="canvasField" id="canvasField" value=""/>
          </form>
          <div id="target" class="recipe" style="padding: 40px;">
            <div class="row">
              <div class="col s6 offset-s3">
                <input id="main_title" class="input" type="text" name="main_title"/>
                <label for="main_title"> Hlavička jídelníčku </label>
              </div>
            </div>
          </form>
        </div>
        <div class="col s12">
          <button class="btn right" onclick="capture();" type="submit" value="Preview"> Zkonvertovat </button>
        </div>
      </div>
    </div>
    <?php
      require_once "../../admin/resources/maintance/components/alerts.php";
     ?>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="../../admin/resources/materialize/js/materialize.min.js"></script>
    <script type="text/javascript">
      function capture() {
        var element = $('#target');
          $('.input').each(function(){
            let v = $(this).val();
            $(this).replaceWith("<p>" +v+ "</p>");
            $('label').remove();
            $('.new_item').remove();
            $('.new_category').remove();
          });

        $('#target').html2canvas({
          onrendered: function (canvas) {
                    //Set hidden field's value to image data (base-64 string)
            $('#img_val').val(canvas.toDataURL("image/png"));
            $('#canvasField').val(canvas);
                    //Submit the form manually
            document.getElementById("myForm").submit();
          }
        });
      }
    </script>
  </div>
  </body>
</html>
