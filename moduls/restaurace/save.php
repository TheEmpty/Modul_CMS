<?php
  session_start();
  if(empty($_SESSION["usr"])){
    header("Location: ../index.php");
  }
  require_once "../../admin/config.php";

  //PHP Mailer


  // Import PHPMailer classes into the global namespace
  // These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require_once "../../vendor/autoload.php";

  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  if(isset($_POST["submit"])){
    if(!empty($_POST["mail"])){
      try {
          //Server settings
          $mail->SMTPDebug = 2;                                 // Enable verbose debug output
          $mail->isSMTP();                                      // Set mailer to use SMTP
          $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
          $mail->SMTPAuth = true;
          $mail->isHTML(true);                        // Enable SMTP authentication
          $mail->Username = 'testerkocvara@gmail.com';                 // SMTP username
          $mail->Password = 'shipuden285';                           // SMTP password
          $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
          $mail->Port = 465;                                    // TCP port to connect to

          //Recipients
          $mail->setFrom('testerkocvara@gmail.com', 'John');
          $mail->addAddress($_POST["mail"]);     // Add a recipient
        //  $mail->addAddress('ellen@example.com');               // Name is optional
      //    $mail->addReplyTo('info@example.com', 'Information');
        //  $mail->addCC('cc@example.com');
        //  $mail->addBCC('bcc@example.com');

          //Attachments
          $mail->addAttachment('img.png', 'img.png');         // Add attachments
        //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

          //Content
          $mail->isHTML(true);                                  // Set email format to HTML
          $mail->Subject = 'This is a test email';

          $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
          $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

          $mail->send();
          echo 'Message has been sent';
      } catch (Exception $e) {
          echo 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
      }
    }else{
      echo "Nevyplnili jste email";
    }
  }

  $app = new APP($db);
  $user = unserialize($_SESSION["usr"]);
  $user->inheritPDO($db);

  //Get the base-64 string from data
  $filteredData=substr($_POST['img_val'], strpos($_POST['img_val'], ",")+1);
  //Decode the string
  $unencodedData=base64_decode($filteredData);
  //Save the image
  file_put_contents('img.png', $unencodedData);
?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../../admin/resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="library/html2canvas.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="css/recipe.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Administrace | M_CMS </title>
    <script type="text/javascript">
      $(document).ready(function(){
        var a = $('<a>')
          .attr('download', 'img.png')
          .appendTo('body');

          a[0].click();
          a.remove();
        });
    </script>
  </head>
  <body>
    <div class="page row">
      <div class="col s12">
        <h2> Výsledek konvertu </h2>
        <p> Výsledek zhotovení jídelního lístku je níže, pokud si přejete obrázek stáhnout, klikněte, prosím, na tlačítko "stáhnout".</p>
        <a href="img.png" download="img.png" id="btn-Convert-Html2Image" class="waves-effect waves-green btn-flat"> <button class="btn"> Stáhnout </button> </a>
        <form method="post">
          <div class="input-field col s6">
            <input id="mail" type="email" name="mail" placeholder="adresu@prijemce.cz"/>
          </div>
          <button id="send" name="submit" type="submit" class="btn"> Pošli mail </button>
        </form>
      </div>
    </div>
    <?php
    //Show the image
    echo '<img id="canvased" src="'.$_POST['img_val'].'" />';
    ?>
    <script type="text/javascript" src="../../admin/resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
