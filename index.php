<?php
  session_start();
  if(!empty($_SESSION["usr"])){
    header("Location: admin/index.php");
  }
  require_once "admin/config.php";
 ?>
<!DOCTYPE html>
<html lang="cs">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="admin/resources/materialize/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title> Modul_cms </title>
    <script type="text/javascript">
      $(document).ready(function(){
        $(document.body).on('click', '.login_btn', function(){
          $(".page").empty().hide();
          $(".page").load("admin/resources/maintance/pages/login.php").fadeIn("slow");
        });

        $(document.body).on('click', '.reg_btn', function(){
          $(".page").empty().hide();
          $(".page").load("admin/resources/maintance/pages/registration.php").fadeIn("slow");
        });
      });
    </script>
  </head>
  <body>
    <div class="page">
      <?php
        include_once "admin/resources/maintance/pages/login.php";
        require_once "admin/resources/maintance/components/alerts.php";
      ?>
    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="admin/resources/materialize/js/materialize.min.js"></script>
  </body>
</html>
